<?php

/**
 * This library bridges League\Container functionality for use with Slim 3.
 *
 * @license http://opensource.org/licenses/MIT MIT
 */
namespace ckrack\SlimLeagueContainerBridge;

use League\Container\ServiceProvider\AbstractServiceProvider;
use Slim\CallableResolver;
use Slim\Collection;
use Slim\Handlers\Error;
use Slim\Handlers\PhpError;
use Slim\Handlers\NotAllowed;
use Slim\Handlers\NotFound;
use Slim\Handlers\Strategies\RequestResponse;
use Slim\Http\Environment;
use Slim\Http\Headers;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Router;

final class ServiceProvider extends AbstractServiceProvider
{
    /**
     * @var array
     */
    protected $provides = [
        'settings',
        'environment',
        'request',
        'response',
        'router',
        'foundHandler',
        'errorHandler',
        'notFoundHandler',
        'notAllowedHandler',
        'callableResolver',
        'phpErrorHandler'
    ];

    /**
     * @var array
     */
    private $defaultSettings = [
        'httpVersion' => '1.1',
        'responseChunkSize' => 4096,
        'outputBuffering' => 'append',
        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails' => false
    ];

    private $userSettings = [];

    /**
     * {@inheritdoc}
     */
    public function __construct($userSettings = [])
    {
        // merge userSettings and defaultSettings
        $this->userSettings = $userSettings;
    }

    /**
     * {@inheritdoc}
     */
    public function register()
    {
        $this->getContainer()->share('settings', function () {
            // initialise settings with merged usersettings.
            return new Collection(array_merge($this->defaultSettings, $this->userSettings));
        });

        /**
         * This service MUST return a shared instance
         * of \Slim\Interfaces\Http\EnvironmentInterface.
         *
         * @return EnvironmentInterface
         */
        $this->getContainer()->share('environment', function () {
            return new Environment($_SERVER);
        });

        /**
         * PSR-7 Request object
         *
         * @return ServerRequestInterface
         */
        $this->getContainer()->share('request', function () {
            return Request::createFromEnvironment($this->getContainer()->get('environment'));
        });

        /**
         * PSR-7 Response object
         *
         * @return ResponseInterface
         */
        $this->getContainer()->share('response', function () {
            $headers = new Headers(['Content-Type' => 'text/html; charset=UTF-8']);
            $response = new Response(200, $headers);

            return $response->withProtocolVersion($this->getContainer()->get('settings')['httpVersion']);
        });

        /**
         * This service MUST return a SHARED instance
         * of \Slim\Interfaces\RouterInterface.
         *
         * @return RouterInterface
         */
        $this->getContainer()->share('router', function () {
            $router = new Router();

            $routerCacheFile = false;
            $settings = $this->getContainer()->get('settings');
            if (isset($settings['routerCacheFile'])) {
                $routerCacheFile = $this->getContainer()->get('settings')['routerCacheFile'];
            }

            $router = (new Router)->setCacheFile($routerCacheFile);
            if (method_exists($router, 'setContainer')) {
                $router->setContainer($this->getContainer());
            }

            return $router;
        });

        /**
         * This service MUST return a SHARED instance
         * of \Slim\Interfaces\InvocationStrategyInterface.
         *
         * @return InvocationStrategyInterface
         */
        $this->getContainer()->share('foundHandler', function () {
            return new RequestResponse();
        });


        /**
         * This service MUST return a callable
         * that accepts three arguments:
         *
         * 1. Instance of \Psr\Http\Message\ServerRequestInterface
         * 2. Instance of \Psr\Http\Message\ResponseInterface
         * 3. Instance of \Exception
         *
         * The callable MUST return an instance of
         * \Psr\Http\Message\ResponseInterface.
         *
         * @return callable
         */
        $this->getContainer()->share('errorHandler', function () {
            return new Error($this->getContainer()->get('settings')['displayErrorDetails']);
        });

        /**
         * This service MUST return a callable
         * that accepts three arguments:
         *
         * 1. Instance of \Psr\Http\Message\ServerRequestInterface
         * 2. Instance of \Psr\Http\Message\ResponseInterface
         * 3. Instance of \Error
         *
         * The callable MUST return an instance of
         * \Psr\Http\Message\ResponseInterface.
         *
         * @return callable
         */
        $this->getContainer()->share('phpErrorHandler', function () {
            return new PhpError($this->getContainer()->get('settings')['displayErrorDetails']);
        });

        /**
         * This service MUST return a callable
         * that accepts two arguments:
         *
         * 1. Instance of \Psr\Http\Message\ServerRequestInterface
         * 2. Instance of \Psr\Http\Message\ResponseInterface
         *
         * The callable MUST return an instance of
         * \Psr\Http\Message\ResponseInterface.
         *
         * @return callable
         */
        $this->getContainer()->share('notFoundHandler', function () {
            return new NotFound();
        });

        /**
         * This service MUST return a callable
         * that accepts three arguments:
         *
         * 1. Instance of \Psr\Http\Message\ServerRequestInterface
         * 2. Instance of \Psr\Http\Message\ResponseInterface
         * 3. Array of allowed HTTP methods
         *
         * The callable MUST return an instance of
         * \Psr\Http\Message\ResponseInterface.
         *
         * @return callable
         */
        $this->getContainer()->share('notAllowedHandler', function () {
            return new NotAllowed();
        });

        /**
         * Instance of \Slim\Interfaces\CallableResolverInterface
         *
         * @param Container $container
         *
         * @return CallableResolverInterface
         */
        $this->getContainer()->share('callableResolver', function () {
            return new CallableResolver($this->getContainer());
        });
    }
}
