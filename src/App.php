<?php

/**
 * This library bridges League\Container functionality for use with Slim 3.
 *
 * @license http://opensource.org/licenses/MIT MIT
 */
namespace ckrack\SlimLeagueContainerBridge;

use Interop\Container\ContainerInterface;
use League\Container\Container;
use League\Container\ReflectionContainer;
use InvalidArgumentException;

class App extends \Slim\App
{
    /**
     * Create new application
     *
     * @param ContainerInterface|array $container Either a ContainerInterface or an associative array of app settings
     * @throws InvalidArgumentException when no container is provided that implements ContainerInterface
     */
    public function __construct($container = [])
    {
        $userSettings = [];

        if (is_array($container)) {
            // an array with settings was passed, save it for later use (passing it to ServiceProvider)
            $userSettings = isset($container['settings']) ? $container['settings'] : [];
            // create Container object
            $container = new Container;
        }

        if (!$container instanceof ContainerInterface) {
            throw new \InvalidArgumentException('Expected a ContainerInterface');
        }

        // enable auto-wiring
        $container->delegate(new ReflectionContainer);

        // add Slim specific servic provider and pass the usersettings
        $container->addServiceProvider(new ServiceProvider($userSettings));

        parent::__construct($container);
    }
}
