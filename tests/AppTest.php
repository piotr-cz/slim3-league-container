<?php

namespace ckrack\SlimLeagueContainerBridge\Tests;

use ckrack\SlimLeagueContainerBridge\App;
use League\Container\Container;
use PHPUnit\Framework\TestCase;

/**
 * Basic tests for the App class
 */
class AppTest extends TestCase
{
    protected $config = [
        'settings' => [
            'foo' => 'bar'
        ]
    ];

    public function testGetAppContainer()
    {
        $app = new App;
        $this->assertInstanceOf('League\\Container\\Container', $app->getContainer());
    }

    public function testGet()
    {
        $container = new Container();
        $container->add('foo', new \StdClass());
        $this->assertInstanceOf('StdClass', $container->get('foo'));
    }

    public function testDefaultSettings()
    {
        $app = new App;
        $settings = $app->getContainer()->get('settings');
        $this->assertEquals('1.1', $settings['httpVersion']);
    }

    public function testUserSettings()
    {
        $app = new App($this->config);
        $settings = $app->getContainer()->get('settings');
        $this->assertEquals('1.1', $settings['httpVersion']);
        $this->assertEquals('bar', $settings['foo']);
    }
}
