<?php

namespace ckrack\SlimLeagueContainerBridge\Tests;

use Slim\Router;

/**
 * This is a Router that differs from Slim\Router only because of the difference in the class name.
 */
class TestCustomRouter extends Router
{
}
