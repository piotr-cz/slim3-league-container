<?php

namespace ckrack\SlimLeagueContainerBridge\Tests;

use ckrack\SlimLeagueContainerBridge\App;
use League\Container\Container;
use PHPUnit\Framework\TestCase;

/**
 * Tests for overriding custom set classes
 */
class CustomOverrideTest extends TestCase
{

    public function testCustomEnvironment()
    {
        // this is the default from index.php
        $app = new App;

        $container = $app->getContainer();
        $container->share('environment', function () use ($container) {
            return new TestCustomEnvironment($_SERVER);
        });

        $this->assertInstanceOf('ckrack\SlimLeagueContainerBridge\Tests\TestCustomEnvironment', $container->get('environment'));
        $this->assertInstanceOf('ckrack\SlimLeagueContainerBridge\Tests\TestCustomEnvironment', $app->getContainer()->get('environment'));
    }

    public function testCustomRouter()
    {
        $container = new Container;
        $container->share('router', function () use ($container) {
            return new TestCustomRouter;
        });

        $this->assertInstanceOf('ckrack\SlimLeagueContainerBridge\Tests\TestCustomRouter', $container->get('router'));

        $app = new App($container);

        $this->assertInstanceOf('ckrack\SlimLeagueContainerBridge\Tests\TestCustomRouter', $app->getContainer()->get('router'));
    }
}
