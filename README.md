# Slim3 League Container Bridge

This library lets you replace Pimple container in [Slim framework 3](http://www.slimframework.com/) with [League/Container](http://container.thephpleague.com/)

## Installation

`$ composer require ckrack/slim-league-container-bridge`

## Usage

In your `index.php` file:

```use ckrack\SlimLeagueContainerBridge\App;

// load settings
$settings = require __DIR__.'/app/settings.php';

// Instantiate the app
$app = new App($settings);
```

## Testing

Run the tests using the local phpunit in `vendor/bin` to use phpunit.xml settings.

`$ ./vendor/bin/phpunit`
